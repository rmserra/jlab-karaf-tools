/*
 *  Copyright 2014 Jlab S.A.
 *  Todos los derechos reservados
 */
package ar.com.jlab.osgi.karaf.commands;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.karaf.shell.console.OsgiCommandSupport;

/**
 * Clase base para implementar comandos karaf
 *
 * @author Rodrigo Serra <rodrigo.serra@jlab.com.ar>
 */
public abstract class ComandoBase extends OsgiCommandSupport {

    /**
     * Muestra un objeto por la consola. Se invoca con dos array de string, uno
     * de títulos y otro de nombre de propiedades que deben coincidir uno a uno.
     *
     * @param obj objeto a mostrar
     * @param titulos array de títulos
     * @param campos array de nombre de campos
     * @throws Exception si se produce un error
     */
    protected void mostrar(Object obj, String[] titulos, String[] campos) throws Exception {
        mostrar(session.getConsole(), obj, titulos, campos);
    }

    /**
     * Muestra un objeto por un PrintStream. Se invoca con dos array de string,
     * uno de títulos y otro de nombre de propiedades que deben coincidir uno a
     * uno.
     *
     * @param out PrintStream donde mostrar el objeto
     * @param obj objeto a mostrar
     * @param titulos array de títulos
     * @param campos array de nombre de campos
     * @throws Exception si se produce un error
     */
    protected void mostrar(PrintStream out, Object obj, String[] titulos, String[] campos) throws Exception {
        if (titulos.length != campos.length) {
            throw new IllegalArgumentException("La cantidad de elementos de titulos debe ser igual a la de campos");
        }
        int anchoTitulos = 10;
        String[] datos = new String[campos.length];
        for (int i = 0; i < campos.length; i++) {
            if (anchoTitulos < titulos[i].length()) {
                anchoTitulos = titulos[i].length();
            }
            datos[i] = BeanUtils.getProperty(obj, campos[i]);
            if (datos[i] == null) {
                datos[i] = "";
            }
        }

        String fmt = "%-" + anchoTitulos + "." + anchoTitulos + "s : %s\n";
        for (int i = 0; i < campos.length; i++) {
            out.printf(fmt, titulos[i], datos[i]);
        }
    }

    /**
     * Muestra una tabla con los datos ordenados de una lista de objetos. Se
     * invoca con dos array de string, uno de títulos y otro de nombre de
     * propiedades que deben coincidir uno a uno.
     *
     * @param objs Lista de objetos a mostrar
     * @param titulos array de títulos
     * @param campos array de nombre de campos
     * @param orden array de enteros con los numero de columna que ordenan
     * @throws Exception si se produce un error
     */
    protected void listar(List objs, String[] titulos, String[] columnas, int[] orden) throws Exception {
        listar(session.getConsole(), objs, titulos, columnas, orden);

    }

    /**
     * Muestra una tabla con los datos de una lista de objetos. Se invoca con
     * dos array de string, uno de títulos y otro de nombre de propiedades que
     * deben coincidir uno a uno.
     *
     * @param objs Lista de objetos a mostrar
     * @param titulos array de títulos
     * @param campos array de nombre de campos
     * @throws Exception si se produce un error
     */
    protected void listar(List objs, String[] titulos, String[] columnas) throws Exception {
        listar(session.getConsole(), objs, titulos, columnas, null);
    }

    /**
     * Muestra una tabla en un PrintStream con los datos de una lista de
     * objetos. Se invoca con dos array de string, uno de títulos y otro de
     * nombre de propiedades que deben coincidir uno a uno.
     *
     * @param out PrintStream donde mostrar la lista
     * @param objs Lista de objetos a mostrar
     * @param titulos array de títulos
     * @param campos array de nombre de campos
     * @param orden array de enteros con los numero de columna que ordenan
     * @throws Exception si se produce un error
     */
    protected void listar(PrintStream out, List objs, String[] titulos, String[] campos, int[] orden) throws Exception {
        if (titulos.length != campos.length) {
            throw new IllegalArgumentException("La cantidad de elementos de titulos debe ser igual a la de columnas");
        }

        if (objs != null && objs.size() > 0) {
            List<Object[]> aMostrar = new ArrayList<>();

            for (Object o : objs) {
                String[] valores = new String[campos.length];
                for (int i = 0; i < campos.length; i++) {
                    valores[i] = BeanUtils.getProperty(o, campos[i]);
                }
                aMostrar.add(valores);
            }
            listar(out, aMostrar, titulos, orden);
        }
    }

    /**
     * Muestra una tabla con los datos de una lista de objetos.
     *
     * @param objs Lista de objetos a mostrar
     * @param campos array de nombre de campos
     * @throws Exception si se produce un error
     */
    protected void listar(List<Object[]> datos, String[] columnas) {
        listar(datos, columnas, (int[]) null);
    }

    /**
     * Muestra una tabla con los datos ordenados de una lista de objetos.
     *
     * @param objs Lista de objetos a mostrar
     * @param campos array de nombre de campos
     * @param orden array de enteros con las columnas que ordena los datos
     * @throws Exception si se produce un error
     */
    protected void listar(List<Object[]> datos, String[] columnas, int[] orden) {
        listar(session.getConsole(), datos, columnas, orden);
    }

    /**
     * Muestra una tabla en un PrintStream con los datos de una lista ordenada
     * de objetos. Se invoca con dos array de string, uno de títulos y otro de
     * nombre de propiedades que deben coincidir uno a uno.
     *
     * @param out PrintStream donde mostrar la lista
     * @param datos Lista de objetos a mostrar
     * @param titulos array de títulos
     * @param campos array de nombre de campos
     * @param orden array de enteros con los numero de columna que ordenan
     * @throws Exception si se produce un error
     */
    protected void listar(PrintStream out, List<Object[]> datos, String[] titulos, final int[] orden) {
        if (datos != null && datos.size() > 0) {
            int[] anchos = new int[titulos.length];
            for (int i = 0; i < anchos.length; i++) {
                anchos[i] = 10;
            }
            for (Object[] o : datos) {
                for (int i = 0; i < titulos.length; i++) {
                    String s = o[i] != null ? o[i].toString() : null;
                    if (s != null) {
                        for (String s1 : s.split("\n")) {
                            anchos[i] = anchos[i] < s1.length() ? s1.length() : anchos[i];
                        }
                    }
                }
            }
            String fmt = "";
            String separacion = "";
            for (int ancho : anchos) {
                fmt += (fmt.length() == 0 ? "" : " | ") + "%-" + ancho + "." + ancho + "s";
                separacion += (separacion.length() == 0 ? "" : "-+-")
                        + StringUtils.repeat("-", ancho);
            }
            fmt += "\n";
            out.printf(fmt, titulos);
            out.printf("%s\n", separacion);

            List<String[]> aMostrar = new LinkedList<>();
            for (Object[] o : datos) {
                String[] valores = new String[titulos.length];
                for (int i = 0; i < o.length && i < titulos.length; i++) {
                    String s = o[i] != null ? o[i].toString() : "";
                    valores[i] = s;
                }
                aMostrar.add(valores);
            }
            if (orden != null) {
                Collections.sort(aMostrar, new Comparator<String[]>() {
                    @Override
                    public int compare(String[] o1, String[] o2) {
                        int res = 0;
                        for (int i : orden) {
                            int cmp = o1[i].compareTo(o2[i]);
                            if (cmp != 0) {
                                res = cmp;
                                break;
                            }
                        }
                        return res;
                    }
                });
            }
            for (String[] v : aMostrar) {
                String[][] lineas = new String[v.length][];

                int max = 0;
                for (int i = 0; i < v.length; i++) {
                    lineas[i] = v[i].split("\n");
                    if (lineas[i].length > max) {
                        max = lineas[i].length;
                    }
                }

                String[] li = new String[v.length];
                for (int l = 0; l < max; l++) {
                    for (int i = 0; i < v.length; i++) {
                        li[i] = l < lineas[i].length ? lineas[i][l] : "";
                    }
                    out.printf(fmt, li);
                }
                if (max > 1) {
                    out.printf("%s\n", separacion);
                }
            }
        }
    }

    /**
     * Muestra un mensaje por al consola
     *
     * @param fmt formato al estilo printf
     * @param args argumentos
     */
    protected void mensaje(String fmt, Object... args) {
        if (args == null || args.length == 0) {
            session.getConsole().print(fmt);
        } else {
            session.getConsole().format(fmt, args);
        }
    }

    /**
     * Formatea el tiempo
     *
     * @param milisegundos milisegundos
     * @return tiempo formateado
     */
    protected static String formatearTiempo(long milisegundos) {
        return String.format("%02d:%02d:%02d.%03d",
                (milisegundos / 1000 / 60 / 60) % 60,
                (milisegundos / 1000 / 60) % 60,
                (milisegundos / 1000) % 60,
                milisegundos % 1000);
    }

    /**
     * Formatea una hora y fecha
     *
     * @param fecha fecha y hora a mostrar
     * @return fecha y hora formateada
     */
    protected static String formatearHoraFecha(Date fecha) {
        return String.format("%1$te/%1$tm/%1$tY %1$tH:%1$tM:%1$tS", fecha);
    }
}
